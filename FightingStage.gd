extends Node2D

var enums = preload("res://enums.gd")

var animation
var audio
var audio_hit1 = preload("res://assets/sfx/hit1.wav")
var audio_hit2 = preload("res://assets/sfx/hit2.wav")
var audio_boom1 = preload("res://assets/sfx/boom1.wav")
var audio_boom2 = preload("res://assets/sfx/boom2.wav")

# Called when the node enters the scene tree for the first time.
func _ready():
	connect_ui()
	animation = $CanvasLayer/MarginContainer/GUI/AnimationPlayer
	audio = $CanvasLayer/MarginContainer/AudioStreamPlayer
	$Player.pause()
	$Enemy.pause()
	$CanvasLayer/Button.connect("pressed", self, "_on_Intro_start_game")
	
func _on_Intro_start_game():
	$CanvasLayer/Button.hide()
	$CanvasLayer/TextureRect.hide()
	$CanvasLayer/MarginContainer.show()
	$Player.unpause()
	$Enemy.unpause()
	$CanvasLayer/MarginContainer/AudioStreamPlayer2.play()
	

func connect_ui():
	# Connect CPU Activity for Enemy
	var progress_bar = $CanvasLayer/MarginContainer/GUI/EnemyInfo/LeftStats/CPU/MarginContainer/StatusBar
	progress_bar.set_values("CPU", $Enemy/CPU.max_activity)
	
	$Enemy/CPU.connect("cpu_damaged", progress_bar, "_on_value_changed")
	
	# Connect Weapon1 for Enemy
	progress_bar = $CanvasLayer/MarginContainer/GUI/EnemyInfo/LeftStats/W1/MarginContainer/StatusBar
	progress_bar.set_values($Enemy/Weapon.weapon_name, $Enemy/Weapon.max_integrity)
	
	$Enemy/Weapon.connect("weapon_damaged", progress_bar, "_on_value_changed")
	
	
	
	# Connect Cooldown for Enemy
	progress_bar = $CanvasLayer/MarginContainer/GUI/EnemyInfo/CenterStats/Cooldown/MarginContainer/StatusBar
	progress_bar.set_values("Cooldown", $Enemy.cooldown)
	
	$Enemy.connect("cooldown_changed", progress_bar, "_on_value_changed")
	
	# Connect Battery for Enemy
	progress_bar = $CanvasLayer/MarginContainer/GUI/EnemyInfo/RightStats/Battery/MarginContainer/StatusBar
	progress_bar.set_values("Battery Level", $Enemy/Battery.max_energy)
	
	$Enemy/Battery.connect("battery_damaged", progress_bar, "_on_value_changed")
	
	# Connect Battery for Enemy
	progress_bar = $CanvasLayer/MarginContainer/GUI/EnemyInfo/RightStats/W2/MarginContainer/StatusBar
	progress_bar.set_values($Enemy/Weapon2.weapon_name, $Enemy/Weapon2.max_integrity)
	
	$Enemy/Weapon2.connect("weapon_damaged", progress_bar, "_on_value_changed")
	
	# Connect CPU Activity for Player
	progress_bar = $CanvasLayer/MarginContainer/GUI/PlayerInfo/General/MarginContainer/VBoxContainer/CPUStatus
	progress_bar.set_values("CPU", $Player/CPU.max_activity)
	
	$Player/CPU.connect("cpu_damaged", progress_bar, "_on_value_changed")
	
	# Connect Cooldown for Player
	progress_bar = $CanvasLayer/MarginContainer/GUI/PlayerInfo/General/MarginContainer/VBoxContainer/CooldownStatus
	progress_bar.set_values("Cooldown", $Player.cooldown)
	
	$Player.connect("cooldown_changed", progress_bar, "_on_value_changed")
	
	# Connect Battery for Player
	progress_bar = $CanvasLayer/MarginContainer/GUI/PlayerInfo/General/MarginContainer/VBoxContainer/BatteryStatus
	progress_bar.set_values("Battery Level", $Player/Battery.max_energy)
	
	$Player/Battery.connect("battery_damaged", progress_bar, "_on_value_changed")
	
	# Connect Weapon for Player
	progress_bar = $CanvasLayer/MarginContainer/GUI/PlayerInfo/Weapon/MarginContainer/StatusBar
	progress_bar.set_values($Player/Weapon.weapon_name, $Player/Weapon.max_integrity)
	
	$Player/Weapon.connect("weapon_damaged", progress_bar, "_on_value_changed")
	$Player/Weapon.connect("weapon_damaged", self, "_on_weapon_damaged", [progress_bar.get_node("Weapon")])
	
	# Connect Weapon2 for Player
	progress_bar = $CanvasLayer/MarginContainer/GUI/PlayerInfo/Weapon2/MarginContainer/StatusBar
	progress_bar.set_values($Player/Weapon2.weapon_name, $Player/Weapon2.max_integrity)
	
	$Player/Weapon2.connect("weapon_damaged", progress_bar, "_on_value_changed")
	$Player/Weapon2.connect("weapon_damaged", self, "_on_weapon_damaged", [progress_bar.get_node("Weapon2")])
	
	# Connect attack buttons
	for button in get_tree().get_nodes_in_group("attack_buttons"):
    	button.connect("pressed", self, "_on_attack_button_pressed", [button])

	# Connect equip buttons
	for button in get_tree().get_nodes_in_group("equip_buttons"):
    	button.connect("pressed", self, "_on_equip_button_pressed", [button])

	#Connect defence button
	var button = $CanvasLayer/MarginContainer/GUI/PlayerInfo/General/MarginContainer/VBoxContainer/Defence
	button.connect("button_down", self, "_on_defence_button_down")
	button.connect("button_up", self, "_on_defence_button_up")
	
	# Connect ENEMY Action
	$Enemy.connect("action_executed", self, "_on_Enemy_action_executed")
	# Connect Player Action
	$Player.connect("action_executed", self, "_on_Player_action_executed")
	
	# Connect Enemy death
	$Enemy/CPU.connect("cpu_died", self, "_on_Enemy_died")
	$Enemy/Battery.connect("battery_died", self, "_on_Enemy_died")
	
	# Connect Player death
	$Player/CPU.connect("cpu_died", self, "_on_Player_died")
	$Player/Battery.connect("battery_died", self, "_on_Player_died")
	
func _on_equip_button_pressed(button):
	if $Player.equip_button != null:
		$Player.equip_button.get_parent().get_parent().get_parent().modulate = Color(1, 1, 1)
	
	$Player.equip_button = button
	button.get_parent().get_parent().get_parent().modulate = Color(1, 1, .5)
		
func _on_attack_button_pressed(button):
	if $Player.target_button != null:
		$Player.target_button.get_parent().get_parent().get_parent().modulate = Color(1, 1, 1)
	
	$Player.target_button = button
	button.get_parent().get_parent().get_parent().modulate = Color(0.5, 0.5, 1)
		
func _on_defence_button_down():
	$Player.defence_button = true
	
func _on_defence_button_up():
	$Player.defence_button = false
	
func _on_Enemy_action_executed(type, target = null, damage = null):
	match type:
		enums.ACTION_TYPE.ATTACK:
			$Player.take_damage(target, damage)
			if audio.stream != audio_boom2:
				audio.stream = audio_hit1
				audio.play()
			if animation.current_animation != "PlayerDead":
				match target:
					"Weapon":
						animation.play("PlayerWeapon")
					"Weapon2":
						animation.play("PlayerWeapon2")
					"CPU":
						animation.play("PlayerGeneral")
					"Battery":
						animation.play("PlayerGeneral")
			
func _on_Player_action_executed(type, target = null, damage = null):
	match type:
		enums.ACTION_TYPE.ATTACK:
			$Enemy.take_damage(target, damage)
			
			if audio.stream != audio_boom1:
				audio.stream = audio_hit2
				audio.play()
			if animation.current_animation != "EnemyDead":
				match target:
					"Weapon":
						animation.play("WeaponTremble")
					"Weapon2":
						animation.play("Weapon2Tremble")
					"CPU":
						animation.play("CPUTremble")
					"Battery":
						animation.play("BatteryTremble")
				
			# Clear selections
			$Player.target_button.get_parent().get_parent().get_parent().modulate = Color(1, 1, 1)
		enums.ACTION_TYPE.DEFEND:
			$CanvasLayer/MarginContainer/GUI/PlayerInfo/General.modulate = Color(.5, 1, 1)
		enums.ACTION_TYPE.UNDEFEND:
			$CanvasLayer/MarginContainer/GUI/PlayerInfo/General.modulate = Color(1, 1, 1)
	
func _on_weapon_damaged(current_integrity, button):
	if current_integrity < 0.1:
		button.disabled = true
		if $Player.equip_button == button:
			$Player.equip_button.get_parent().get_parent().get_parent().modulate = Color(1, 1, 1)
			$Player.equip_button = null
	else:
		button.disabled = false
		
func _on_Enemy_died():
	$Enemy.die()
	audio.stream = audio_boom1
	audio.play()
	if animation.current_animation != "EnemyDead":
		animation.play("EnemyDead")
	$Player.pause()
	$CanvasLayer/MarginContainer/GameOver/Win.show()
	
func _on_Player_died():
	$Player.die()
	audio.stream = audio_boom2
	audio.play()
	if animation.current_animation != "PlayerDead":
		animation.play("PlayerDead")
	$Enemy.pause()
	$CanvasLayer/MarginContainer/GameOver/Lose.show()
