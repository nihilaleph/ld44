extends Node

signal weapon_died
signal weapon_damaged

export (String) var weapon_name
export (float) var damage
export (float) var cooldown
export (float) var energy_usage
export (float) var max_integrity
var current_integrity

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	current_integrity = max_integrity
	
func take_damage(damage):
	# Weapon get integrity damage
	current_integrity -= damage
	
	# Check if weapon died from damage and signal
	if current_integrity <= 0:
		current_integrity = 0
		emit_signal("weapon_died")
		
	# Signal current weapon level
	emit_signal("weapon_damaged", current_integrity)
	
func heal(damage):
	
	# Weapon heals integrity damage
	current_integrity += damage
	
	# Check if weapon is maxed
	if current_integrity > max_integrity:
		current_integrity = max_integrity
		
	# Signal current weapon level
	emit_signal("weapon_damaged", current_integrity)