extends Node

signal cpu_died
signal cpu_damaged

export (float) var max_activity
var current_activity
export (float) var cooldown_damage_factor
export (float) var initial_cooldown

func _ready():
	current_activity = max_activity
	
func take_damage(damage):
	# CPU takes activity damage
	current_activity -= damage
	
	# Check if CPU died from damage and inform
	if current_activity <= 0.1:
		current_activity = 0
		emit_signal("cpu_died")
		
	# Signal current activity level
	emit_signal("cpu_damaged", current_activity)
	
	# Damage on Cooldown
	get_parent().increase_cooldown(damage * cooldown_damage_factor)

func heal(damage):
	# CPU heals activity damage
	current_activity += damage
	
	# Check if CPU is maxed
	if current_activity > max_activity:
		current_activity = max_activity
	
	# Signal current activity level
	emit_signal("cpu_damaged", current_activity)
