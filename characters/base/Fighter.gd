extends Node

signal cooldown_changed
signal state_changed
signal action_executed

var enums = preload("res://enums.gd")

var cooldown
var current_cooldown setget set_current_cooldown
var state setget set_state
var previous_state

var is_paused = false
var is_dead = false


# Called when the node enters the scene tree for the first time.
func _ready():
	cooldown = $CPU.initial_cooldown
	self.current_cooldown = 0
	state = enums.FIGHTER_STATE.COOLDOWN
	
func _process(delta):
	if is_paused:
		return
	if is_dead:
		return
		
	match state:
		# Update cooldown
		enums.FIGHTER_STATE.COOLDOWN:
			self.current_cooldown += delta
			if current_cooldown > cooldown:
				# Ready for next action
				self.state = enums.FIGHTER_STATE.CALCULATING
		enums.FIGHTER_STATE.CALCULATING:
			calculate_action()
		enums.FIGHTER_STATE.ACTION:
			execute_action()
		enums.FIGHTER_STATE.DEFENDING:
			heal(delta)

# Signal of an attack properties and update Fighter state
func attack(target, weapon):
	# Gather Weapon info
	var weapon_node = get_node(weapon)
	var damage = weapon_node.damage
	var type = enums.ACTION_TYPE.ATTACK
	var energy_consumed = weapon_node.energy_usage
	
	# Signal Stage of executed action
	emit_signal("action_executed", type, target, damage)
	
	# Update Fighter Status
	cooldown = weapon_node.cooldown
	self.current_cooldown = 0
	self.state = enums.FIGHTER_STATE.COOLDOWN
	
	# Consume Battery
	$Battery.drain_energy(energy_consumed)

# Increase cooldown if taken damage
func increase_cooldown(damage):
	cooldown += damage
	
func defend():
	var type = enums.ACTION_TYPE.DEFEND
	
	self.state = enums.FIGHTER_STATE.DEFENDING
	# Signal Stage of executed action
	emit_signal("action_executed", type)

func undefend():
	var type = enums.ACTION_TYPE.UNDEFEND
	
	# Signal Stage of executed action
	emit_signal("action_executed", type)
	
	# Update Fighter Status
	cooldown = $CPU.initial_cooldown
	self.current_cooldown = 0
	self.state = enums.FIGHTER_STATE.COOLDOWN
	
func heal(delta):
	$Battery.drain_energy($Armor.energy_usage * delta)
	$CPU.heal($Armor.recovery * delta)
	$Weapon.heal($Armor.recovery * delta)
	$Weapon2.heal($Armor.recovery * delta)
	
func take_damage(target, damage):
	# Insert damage
	damage *= $Battery.damage_factor
	if state == enums.FIGHTER_STATE.DEFENDING:
		damage *= 1 - $Armor.defence
	get_node(target).take_damage(damage)

##################################################################
#
# Setters and Helpers
#
##################################################################
# Set new state, Signal it, saves previous state (mostly for pause)
func set_state(new_state):
	previous_state = state
	state = new_state
	emit_signal("state_changed", new_state)

# Signal current cooldown and cooldown (in case of change)
func set_current_cooldown(new_cooldown):
	current_cooldown = new_cooldown
	emit_signal("cooldown_changed", current_cooldown, cooldown)

# Return Fighter to previous state (nmostly for Pause)
func return_state():
	state = previous_state
	previous_state = null
	
func pause():
	is_paused = true

func unpause():
	is_paused = false

func die():
	is_dead = true

func revive():
	is_dead = false
	
func calculate_action():
	pass
	
func execute_action():
	pass
