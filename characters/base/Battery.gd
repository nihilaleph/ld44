extends Node

signal battery_died
signal battery_damaged

export (float) var max_energy
export (float) var current_energy
export (float) var factor_multiplier
var damage_factor = 1.0


# Called when the node enters the scene tree for the first time.
func _ready():
	current_energy = max_energy

func take_damage(damage):
	# Increase damage factor
	damage_factor += factor_multiplier * damage
	drain_energy(damage)
	
func drain_energy(damage):
	# Battery takes energy damage
	current_energy -= damage
	
	# Check if battery died from damage and signal
	if current_energy <= 0.1:
		current_energy = 0
		emit_signal("battery_died")
	
	# Signal current battery level
	emit_signal("battery_damaged", current_energy)
	