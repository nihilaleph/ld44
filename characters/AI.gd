extends "base/Fighter.gd"

var target
var weapon
enum POSSIBLE_TARGETS { WEAPON, WEAPON2, CPU, BATTERY, DEFEND }
enum POSSIBLE_WEAPONS { WEAPON, WEAPON2 }

func _ready():
	._ready()

func _on_Timer_timeout():
	$Timer.disconnect("timeout", self, "_on_Timer_timeout")
	
	var random = randi() % POSSIBLE_TARGETS.size()
	
	if $Weapon.current_integrity < 0.1 && $Weapon2.current_integrity < 0.1:
		random = POSSIBLE_TARGETS.DEFEND
	
	match random:
		POSSIBLE_TARGETS.WEAPON:
			target = "Weapon"
		POSSIBLE_TARGETS.WEAPON2:
			target = "Weapon2"
		POSSIBLE_TARGETS.CPU:
			target = "CPU"
		POSSIBLE_TARGETS.BATTERY:
			target = "Battery"
		POSSIBLE_TARGETS.DEFEND:
			defend()
			$Timer.connect("timeout", self, "_on_Timer_timeout_undefend")
			$Timer.wait_time = randi() % 5
			$Timer.start()
			return
			
	random = randi() % POSSIBLE_WEAPONS.size()
	
	match random:
		POSSIBLE_WEAPONS.WEAPON:
			weapon = "Weapon"
			if $Weapon.current_integrity < 0.1:
				weapon = "Weapon2"
		POSSIBLE_WEAPONS.WEAPON2:
			weapon = "Weapon2"
			if $Weapon2.current_integrity < 0.1:
				weapon = "Weapon"
		
	self.state = enums.FIGHTER_STATE.ACTION


func _on_Timer_timeout_undefend():
	undefend()
	$Timer.disconnect("timeout", self, "_on_Timer_timeout_undefend")
	
	
func calculate_action():
	if $Timer.is_stopped():
		$Timer.wait_time = .1
		$Timer.connect("timeout", self, "_on_Timer_timeout")
		$Timer.start()

func execute_action():
	attack(target, weapon)