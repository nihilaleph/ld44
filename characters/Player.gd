extends "base/Fighter.gd"

var target_button = null
var equip_button = null
var defence_button = false

func calculate_action():
	if target_button != null && equip_button != null:
		self.state = enums.FIGHTER_STATE.ACTION
	elif defence_button:
		defend()
		
func heal(delta):
	.heal(delta)
	if !defence_button:
		undefend()
		
func execute_action():
	attack(target_button.name, equip_button.name)
	target_button = null