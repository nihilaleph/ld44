extends VBoxContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func set_values(title, max_value):
	$TextureProgress.max_value = max_value
	$Title.text = title
	$TextureProgress.value = max_value
	pass # Replace with function body.
	
func _on_value_changed(new_value, new_max_value = null):
	if new_max_value != null:
		$TextureProgress.max_value = new_max_value
	$TextureProgress.value = new_value
	var color
	if new_value < 0.1:
		color = Color(0, 0, 0)
		modulate = color
		$Title.modulate = color
		$TextureProgress.modulate = color
	elif $TextureProgress.value == $TextureProgress.max_value:
		color = Color(0, 1, 0)
		$Title.modulate = color
		$TextureProgress.modulate = color
	else:
		color = Color(1, 1, 1)
		modulate = color
		$Title.modulate = color
		$TextureProgress.modulate = color
	
	
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
